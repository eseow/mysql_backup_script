#!/bin/bash

# Copied from:
# http://www.darcynorman.net/2006/03/15/more-on-mysql-backups

# This script created by: Edmund Seow on: 20100322

cd /PATH/TO/BACKUP/DIRECTORY

# Get list of databases from MySQL

# tmpsql$$ creates a temp file for this session, the $$ will translate into a 
# numeric sequence.
cat <<EOF > tmpsql$$
show databases;
EOF

# sed gets rid of the title Database outputted by the show databases command.
/usr/bin/mysql -u root --password=PASSWORD < tmpsql$$ | sed '/Database/d' > dblist$$

#
# Cycle through the list of databases and backup each one. Also gzip the result.

for DB in `cat dblist$$`
do

# Turn off "events" warning message when dumping mysql database
if [ "$DB" = "mysql" ]
then
 /usr/bin/mysqldump -u root --password=PASSWORD --events $DB | gzip --rsyncable > $DB.gz
else
 /usr/bin/mysqldump -u root --password=PASSWORD  --single-transaction $DB | gzip --rsyncable > $DB.gz
fi

done

# Remove the temp files
rm tmpsql$$ dblist$$
